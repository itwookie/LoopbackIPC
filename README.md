# Loopback IPC 

This is a Inter Process Communication Framework using Packages sent over 
the LoopbackAddress. The promise of this Project is to support 
applications across programming languages with maximum abstraction. 

The protocol defined and implemented can be found in the Wiki section of 
this project page. 

To get started you'll want to create a new LIPC instance. It will 
automatically take care of instatiatin a communication server (Port 
6307)
```Java
LIPC ipc = new LIPC(getClass().getPackage());
``` 

Each application will receive a unique AppID, created with a application 
name (Java app can use use the base Package, as these already should 
keep the promise of being project unique) that will be used as source 
throughout the IPC network. 

In order to connect with another application you can turn to the host, 
by using
```Java
//by fetching the server, the server gets to know that we are in the network
ipc.fetchServer();
//the other application might not yet be running, so otherApp could be empty
Optional<AppID> 
otherApp = ipc.findApplication(String applicationBaseIdentifier);
``` 

Other functionality will follow including implementations for NodeJS and 
C/C++ 

If you want to take a look at the source code please switch to the 
respective branch first 

